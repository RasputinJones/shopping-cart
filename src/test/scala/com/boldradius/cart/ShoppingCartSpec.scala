package com.boldradius.cart

import com.boldradius.bundle._
import com.boldradius.item._
import org.scalacheck._
import org.scalatest._
import org.scalatest.prop._

import scala.util.Random


/**
  * Created by uenyioha on 3/20/16.
  */

object dependencies {

  val milk: Product = Product(1, "milk")
  val sugar = Product(2, "sugar")
  val eggs = Product(3, "eggs")
  val margarine = Product(4, "margarine")
  val bread = Product(5, "bread")

  import StoreItem._

  val store = Store (
    1 item milk sellsAt 2.50,
    1 item sugar sellsAt 0.50,
    1 item eggs sellsAt 0.11,
    1 item margarine sellsAt 2.50,
    1 item bread sellsAt 1.10
  )

  val genMilkCart = Gen.containerOf[List, Product](milk)
  val genSugarCart = Gen.containerOf[List, Product](sugar)
  val genEggsCart = Gen.containerOf[List, Product](eggs)
  val genMargarineCart = Gen.containerOf[List, Product](margarine)
  val genBreadCart = Gen.containerOf[List, Product](bread)


  val genMultiProductsCart = Gen.containerOf[List, Product](Gen.oneOf(milk, sugar, eggs, margarine, bread))

  val milkOnlyBundle = Bundles(
    2 items milk bundleSellsAt 2.99
  )

  val eggsOnlyBundle = Bundles(
    (3 items eggs) bundleSellsTheSameAs (2 items eggs)
  )

  val margarineAndBreadBundle = Bundles(
    ((2 items margarine) and (1 items bread)) bundleSellsTheSameAs ((1 item bread) and (1 item margarine))
  )

}

class ShoppingCartSpec extends PropSpec with GeneratorDrivenPropertyChecks with ShouldMatchers {

  import dependencies._

  property("shopping cart calculation for homgenous cart is correct") {
    forAll(genMilkCart) {
      (milkCart: List[Product]) =>
        ShoppingCart(milkCart: _*).totalPrice(store.itemPrices) shouldEqual milkCart.size * store.itemPrices(milk)
    }
  }

  property("shopping cart calculation for heterogenous cart is correct") {
    forAll(genMultiProductsCart) {
      (productsCart: List[Product]) =>
        ShoppingCart(productsCart: _*).totalPrice(store.itemPrices) shouldEqual productsCart.map(store.itemPrices).sum
    }
  }

  property("shopping cart calculation with simple (milk) bundle is correct regardless of # in cart") {
    forAll(genSugarCart, genMilkCart) {
      (sugarCart: List[Product], milkCart: List[Product]) =>
          val cart = Random.shuffle(sugarCart ++ milkCart)
          ShoppingCart(cart: _*).minTotalPriceWithBundles(store.itemPrices, milkOnlyBundle) shouldEqual
            ((sugarCart.size * store.itemPrices(sugar)) +
              (Math.floor(milkCart.size / 2) * BigDecimal(2.99)) + (milkCart.size % 2 * BigDecimal(2.50)))
    }
  }

  property("shopping cart calculation with simple (eggs) bundle is correct regardless of # in cart") {
    forAll(genMilkCart, genEggsCart) {
      (milkCart: List[Product], eggsCart: List[Product]) =>
          val cart = Random.shuffle(milkCart ++ eggsCart)
          ShoppingCart(cart: _*).minTotalPriceWithBundles(store.itemPrices, eggsOnlyBundle) shouldEqual
            (milkCart.size * store.itemPrices(milk)) +
              (Math.floor(eggsCart.size / 3) * 2 * store.itemPrices(eggs)) +
              (eggsCart.size % 3) * store.itemPrices(eggs)
        }
  }

  property("shopping cart calculation with complex (bread and margarine) bundle is correct") {
    forAll(genBreadCart) {
      (breadCart: List[Product]) =>
          val margarineCart = List.fill(breadCart.size * 2)(margarine)
          val cart = Random.shuffle(breadCart ++ margarineCart)
          ShoppingCart(cart: _*).minTotalPriceWithBundles(store.itemPrices, margarineAndBreadBundle) shouldEqual
            breadCart.size * store.itemPrices(bread) + margarineCart.size / 2 * store.itemPrices(margarine)
    }
  }
}
