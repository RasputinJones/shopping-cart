import com.boldradius.bundle.Bundles
import com.boldradius.cart.ShoppingCart
import com.boldradius.item.{Store, StoreItem, Product}

val milk = Product(1, "milk")
val sugar = Product(2, "sugar")
val eggs = Product(3, "eggs")
val margarine = Product(4, "margarine")
val bread = Product(5, "bread")

import StoreItem._

val unitPrices = Store (
  1 item milk sellsAt 2.50,
  1 item sugar sellsAt 0.50,
  1 item eggs sellsAt 0.11,
  1 item margarine sellsAt 2.50,
  1 item bread sellsAt 1.10
)

val bundles = Bundles(
  2 items milk bundleSellsAt 2.99,
  // bundle rules are not transitive so this will not transitively resolve into above
  (3 items milk) bundleSellsTheSameAs (2 items milk),
  (2 items eggs) bundleSellsTheSameAs (1 items eggs),
  ((2 items margarine) and (1 items bread)) bundleSellsTheSameAs ((1 item bread) and (1 item margarine))
)

val newBundles = Bundles(
  ((2 items margarine) and (1 items bread)) bundleSellsTheSameAs ((1 item bread) and (1 item margarine))
)
val cart = ShoppingCart(margarine, margarine, margarine, bread, bread)
val minCost = cart.minTotalPriceWithBundles(unitPrices, newBundles)
val cost = cart.totalPrice(unitPrices)

ShoppingCart.fromItemsAndQuantities((milk, 2), (sugar, 3)).totalPrice(unitPrices)