package com.boldradius.bundle

import com.boldradius._
import com.boldradius.cart.ShoppingCart
import com.boldradius.item._

import scala.language.implicitConversions
import scala.util.Try


/**
  * Created by uenyioha on 3/19/16.
  */

case class Bundle(item: Map[Product, Quantity]) {

  private def bundleDiscount(cart: ShoppingCart)(product: Iterable[Product])
                            (discountFn : BundleQuantites => Discount): Price = {

    val bundleQuantities: BundleQuantites = cart.items.collect {
      case a if product.toSet.contains(a) => a
    }.groupBy(identity)

    discountFn(bundleQuantities)
  }

  private def priceItem(item: Map[Product, Quantity])(unitPriceOf : ItemPrices): Price =
    item.foldLeft(BigDecimal(0.0)) {
      case (acc: Price, (product: Product, qty: Quantity)) => acc + (unitPriceOf(product) * qty)
    }

  def bundleSellsAt(newPrice: Price) : (ItemPrices) => BundleFunction =
    (itemPrices : ItemPrices) => {
      bundleDiscount(_)(item.keys){
        (quantities: BundleQuantites) => {
          val discount : Discount = priceItem(item)(itemPrices) - newPrice
          require (discount >= 0)
          Try {
            quantities.map {
              case (product, list) => Math.floor(list.size / item(product))
            }.min
          }.getOrElse(0.0) * discount
        }
      }
    }

  def bundleSellsTheSameAs(equivalent: Bundle): (ItemPrices) => BundleFunction = {
    require(equivalent.item.forall { case (product, qty) => qty <= item(product) })
    (itemPrices : ItemPrices) => {
      bundleDiscount(_)(item.keys) {
        (quantities: BundleQuantites) => {
          val discount : Discount = priceItem(item)(itemPrices) - priceItem(equivalent.item)(itemPrices)
          require (discount >= 0)
          Try {
            quantities.map {
              case (product, list) => Math.floor(list.size / item(product))
            }.min
          }.getOrElse(0.0) * discount
        }
      }
    }
  }

  def and(product: Bundle) : Bundle = Bundle(this.item ++ product.item)

}

class Bundles()

object Bundles {
  def apply(bundle: ((ItemPrices) => BundleFunction)* ):
    (ItemPrices) => Seq[BundleFunction] = (itemPrices: ItemPrices) => bundle.map(_(itemPrices))
}