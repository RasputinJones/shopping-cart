package com.boldradius.item

import com.boldradius._
import com.boldradius.bundle.Bundle

import scala.language.implicitConversions

/**
  * Created by uenyioha on 3/19/16.
  */


case class Product(id: Int, name: String)

case class StoreItem(product: Product, qty: Quantity) {
  require(qty > 0)

  def sellsAt(price: Price) : Map[Product, Price] = {
    Map(product -> price / qty)
  }
}

object StoreItem {

  implicit class RichItem(val quantity: Quantity) extends AnyVal {
    def item(product: Product) = StoreItem(product, quantity)
    def items(product: Product) = item(product)
  }

  implicit def item2Bundle(item: StoreItem) : Bundle = Bundle(Map(item.product -> item.qty))
}

case class Store(items: Map[Product, Price]*) {

  val itemPrices: Map[Product, Price] = items.reduce(_ ++ _)

}

