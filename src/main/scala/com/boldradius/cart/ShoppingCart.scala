package com.boldradius.cart

import com.boldradius._
import com.boldradius.item._

/**
  * Created by uenyioha on 3/19/16.
  */

case class ShoppingCart(items: Product*) {

  def totalPrice(unitPriceOf: ItemPrices): Price =
    items.groupBy(identity).mapValues(_.size).map {
      case (item, qty) => unitPriceOf(item) * qty
    }.sum

  def totalPrice(itemPrices: ItemPrices,
                 bundles: (ItemPrices) => Seq[BundleFunction]) : List[Price] =  {

      val discounts = bundles(itemPrices).map(fxn => fxn(this) )

      val total : Price = totalPrice(itemPrices)
      List.fill(discounts.size)(total).zip(discounts).map {
        case (t, d) => t - d
      }
    }

  def minTotalPriceWithBundles(itemPrices: ItemPrices,
                               bundles: (ItemPrices) => Seq[BundleFunction])
    = totalPrice(itemPrices, bundles).min
}

object ShoppingCart {

  def fromItemsAndQuantities(items: CartItem*) : ShoppingCart =
    ShoppingCart(
      items.foldLeft(List.empty[Product]){
        case (acc, (product, qty)) => acc ++ List.fill(qty)(product)
      } : _*
    )

}