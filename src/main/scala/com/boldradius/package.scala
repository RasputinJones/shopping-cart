package com

import com.boldradius.cart.ShoppingCart
import com.boldradius.item.Product

/**
  * Created by uenyioha on 3/22/16.
  */
package object boldradius {
  type Price = BigDecimal
  type Discount = BigDecimal

  type ItemPrices = Map[Product, Price]
  type BundleQuantites = Map[Product, Seq[Product]]

  type Quantity = Int
  type CartItem = (Product, Quantity)

  type BundleFunction = (ShoppingCart => Discount)
}
