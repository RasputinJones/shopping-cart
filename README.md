### What is this repository for? ###

This exercise is a common problem in e­commerce and brick­and­mortar retail systems.
A customer shops from some form of catalog, selecting items and quantities they wish to
purchase. When they are ready, they “check out”, that is, complete their purchase, at which
point they are given a total amount of money they owe for the purchase of a specific set of
items.

In the bounds of this problem, certains groups of items can be taken together as a “bundle” with
a different price. E.g. if I buy a single apple in isolation it costs $1.99, if I buy two apples it’s
$2.15. More complex combinations are possible ­ e.g. a loaf of bread “A” purchased with two
sticks of margarine “B” and the second stick of margarine is free (e.g. $0). The same item may
appear in more than one bundle, e.g. any one “cart” of items might be able to be combined in
more than one way.

For this exercise, produce an API and implementation for a service that accepts a collection of
items the customer wishes to purchase (e.g. items and quantities), and produces the lowest
possible price for that collection of items. The API is to be called by other applications in the
same JVM, e.g. don’t worry about providing a REST or other remote interface to the API, just
the actual method calls is fine.

The API is initialized with information that provides all the possible bundles and the catalog of
items and their prices. Once it is initialized, many calls can be made at once to the API to
produce a total price for collections of items, and it should be able to handle multiple
simultaneous calls without errors (e.g. if I’m computing the price for one call, that should not
interfere with computing the price for another call).

Use what you would consider good Scala style. We are not expecting conformance to any
specific coding standard for this exercise, although you should be consistent throughout the
exercise.

Solution
--------

### API Usage ###

1. Create products

```
#!scala
  val milk: Product = Product(1, "milk")
  val sugar = Product(2, "sugar")
  val eggs = Product(3, "eggs")
  val margarine = Product(4, "margarine")
  val bread = Product(5, "bread")
```

1. Price products by adding items to the store shelf using the store addition DSL

```
#!scala
val storeItems = Store (
  1 item milk sellsAt 2.50,
  1 item sugar sellsAt 0.50,
  1 item eggs sellsAt 0.11,
  1 item margarine sellsAt 2.50,
  1 item bread sellsAt 1.10,
)
```

1. Define Bundles using the bundles DSL

```
#!scala
val bundles = Bundles(
  2 items milk bundleSellsAt 2.99,
  // bundle rules are not transitive so this will not transitively resolve into above
  (3 items milk) bundleSellsTheSameAs (2 items milk),
  (2 items eggs) bundleSellsTheSameAs (1 items eggs),
  ((2 items margarine) and (1 items bread)) bundleSellsTheSameAs ((1 item bread) and (1 item margarine))
)
```

1. Create shopping cart and use bundles if you want

```
#!scala
val cart = ShoppingCart(margarine, margarine, margarine, bread, bread) // or ShoppingCart.fromItemsAndQuantities((margarine, 3), (bread, 2))
val totalPrice = cart.totalPrice(unitPrices)
val minTotal = cart.minTotalPriceWithBundles(unitPrices, bundles)
```